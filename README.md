# Mail to Issue

Smol program that converts incoming mails to GitLab Issues.

## Building
Ensure you have the Rust compiler and cargo installed.

```sh
cargo buid
```

## Running Localy

```sh
export CLIENT_TOKEN="<token>" # Gitlab Token
export IMAP_DOMAIN="mail.hs-mannheim.de"
export IMAP_USER="<MatrNmr>@stud.hs-mannheim.de"
export IMAP_PASSWORD="<password>"

./mail_to_issue
```

If you don't want to have your credentials in the environment, you can create an `.env` file containing the values.

## Docker

A docker image is automatically build in the CI. Pass the above values to the container via the `-e` flag.
