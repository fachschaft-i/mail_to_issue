use dotenv::dotenv;
use log::{debug, error, info};
use mail_parser::HeaderValue::Address;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::thread;
use std::time::Duration;

mod gitlab;
mod mail;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    match dotenv() {
        Ok(_) => {}
        Err(e) => error!("No variables loaded from '.env':{:?}", e),
    };
    env_logger::init();

    let gclient = loop {
        match gitlab::Client::new(String::from("fachschaft-i/mails")) {
            Ok(client) => {
                info!("Connected to GitLab");
                break client;
            }
            Err(e) => {
                error!(
                    "Connection to gitlab failed, retrying in 30 Seconds\nMessage: {:?}",
                    e
                );
            }
        }
    };

    loop {
        match run(&gclient) {
            Ok(()) => {
                thread::sleep(Duration::from_secs(60 * 10));
            }
            Err(e) => {
                error!("{:?}", e);
                thread::sleep(Duration::from_secs(30));
            }
        }
    }
}

fn read_uid() -> Result<u64, Box<dyn std::error::Error>> {
    let data_dir = std::env::var("DATA_DIR").unwrap_or_else(|_| String::from("data"));
    let mut path = PathBuf::from(data_dir);
    path.push("last_uid");
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let res = contents.trim().parse::<u64>()?;
    Ok(res)
}

fn write_uid(uid: u32) -> Result<(), std::io::Error> {
    let data_dir = std::env::var("DATA_DIR").unwrap_or_else(|_| String::from("data"));
    let mut path = PathBuf::from(data_dir);
    path.push("last_uid");
    let mut file = File::create(path)?;
    file.write_all(uid.to_string().as_bytes())?;

    Ok(())
}

fn run(client: &gitlab::Client) -> Result<(), Box<dyn std::error::Error>> {
    let uid = match read_uid() {
        Ok(uid) => uid,
        Err(e) => {
            error!("{}", e);
            0
        }
    };

    let mails = mail::fetch_inbox_from(uid + 1)?;
    if let Some((mails, latest_uid)) = mails {
        for mail in mails {
            let parsed_mail = mail_parser::Message::parse(&mail[..]).unwrap();

            let date = parsed_mail.get_date().unwrap();
            let mut title = format!(
                "[{}-{}-{} {}:{}]",
                date.year, date.month, date.day, date.hour, date.minute
            );
            title.push_str(parsed_mail.get_subject().unwrap());

            let mut description = match parsed_mail.get_from() {
                Address(addr) => {
                    let mut res = String::new();
                    if let Some(name) = &addr.name {
                        res.push_str(&name.clone());
                    }
                    if let Some(address) = &addr.address {
                        res.push_str(&format!(" <{}>", address));
                    }
                    format!("From: {res}\n\n")
                }
                something => {
                    format!("From: {:?}\n\n", something)
                }
            };
            let mail_body = String::from(parsed_mail.get_text_body(0).unwrap());
            description.push_str(&mail_body);

            let issue = gitlab::Issue {
                title,
                description,
                due: None,
            };

            debug!("Creating issue: {:?}", issue);
            client.create_mail_issue(&issue)?;
        }
        write_uid(latest_uid)?;
    }

    Ok(())
}
