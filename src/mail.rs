use log::{debug, info};

type Mail = Vec<u8>;
type MailCollection = (Vec<Mail>, u32);
pub fn fetch_inbox_from(uid: u64) -> Result<Option<MailCollection>, Box<dyn std::error::Error>> {
    debug!("Starting to fetch new mails");

    let domain = std::env::var("IMAP_DOMAIN").expect("IMAP_DOMAIN not set");
    let tls = native_tls::TlsConnector::builder().build().unwrap();

    let client = imap::connect((domain.clone(), 993), domain, &tls).unwrap();

    let username = std::env::var("IMAP_USER").expect("IMAP_USER not set");
    let password = std::env::var("IMAP_PASSWORD").expect("IMAP_PASSWORD not set");
    let mut imap_session = client.login(username, password).map_err(|e| e.0)?;

    imap_session.select("INBOX")?;

    let messages = imap_session.uid_search(format!("SUBJECT [I.fachschaft] UID {uid}:* "))?;
    let mut messages: Vec<u32> = messages.into_iter().filter(|m| m > &(uid as u32)).collect();

    if !messages.is_empty() {
        // This ensures that early messages come first
        messages.sort_unstable();

        let latest_uid = *(*messages.last().as_ref().unwrap());

        let messages: String = messages
            .into_iter()
            .map(|m| m.to_string())
            .collect::<Vec<String>>()
            .join(",");

        let messages = imap_session.uid_fetch(messages, "RFC822")?;

        let mails: Vec<Vec<u8>> = messages
            .into_iter()
            .map(|message| Vec::from(message.body().take().unwrap()))
            .collect();

        info!("Fetched {} new mails", mails.len());

        imap_session.logout()?;

        Ok(Some((mails, latest_uid)))
    } else {
        debug!("Fetched no new mails");
        Ok(None)
    }
}
