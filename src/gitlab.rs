use chrono::NaiveDate;
use gitlab::api::{self, projects, Query};
use gitlab::Gitlab;
use serde::Deserialize;

#[derive(Deserialize, Clone, Debug)]
pub struct Issue {
    pub title: String,
    pub description: String,
    pub due: Option<NaiveDate>,
}

pub struct Client {
    client: Gitlab,
    project: String,
}

impl Client {
    pub fn new(project: String) -> Result<Self, Box<dyn std::error::Error>> {
        let client = Gitlab::new(
            "gitlab.com",
            std::env::var("CLIENT_TOKEN").expect("CLIENT_TOKEN not set"),
        )?;

        Ok(Client { client, project })
    }

    pub fn create_mail_issue(&self, issue: &Issue) -> Result<(), Box<dyn std::error::Error>> {
        let endpoint = projects::issues::CreateIssue::builder()
            .project(&*self.project)
            .title(&issue.title)
            .description(&issue.description)
            .label("Mail")
            .build()?;

        api::ignore(endpoint).query(&self.client)?;
        Ok(())
    }
}
