FROM rust:latest as builder

RUN USER=root cargo new --bin mail_to_issue
WORKDIR ./mail_to_issue
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --release
RUN rm src/*.rs

ADD . ./

RUN rm ./target/release/deps/mail_to_issue*
RUN cargo build --release

FROM debian:buster-slim

RUN apt-get update -y -qq && apt-get install openssl ca-certificates -y -qq

ARG APP=/usr/src/app
ENV APP_USER=appuser

RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

RUN mkdir "/data" && chown $APP_USER:$APP_USER "/data"

COPY --from=builder /mail_to_issue/target/release/mail_to_issue ${APP}/mail_to_issue

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}

ENV DATA_DIR="/data"

CMD ["./mail_to_issue"]
